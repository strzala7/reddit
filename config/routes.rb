Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  root 'posts#main'
  devise_for :users, :controllers => {:sessions => "sessions"}
  resources :users do
    get :followers
    get :followed_people
    post "follow", to: "following_relationships#create"
    post "unfollow", to: "following_relationships#destroy"
    resources :achievements, only: [:index]
    post 'add_to_blacklist', to: 'blacklists#create'
    post 'remove_from_blacklist', to: 'blacklists#destroy'
    resources :bans, only: [:new, :create]
  end
  resources :posts, only: [:index, :new, :create, :edit, :update, :destroy]
  get :main, to: 'posts#main'
  get :new, to: 'posts#latest', as: 'latest'
  get :hot, to: 'posts#hot'
  get :favourites, to: 'posts#favourites'
  get ':category_name', to: 'categories#show', as: :categories_show
  put ':category_name', to: 'categories#update', as: :categories_update
  delete ':category_name', to: 'categories#destroy', as: :categories_delete
  scope ':category_name', as: :categories_show do
    post 'add_to_favourites', to: 'favourite_categories#create'
    post 'remove_from_favourites', to: 'favourite_categories#destroy'
    resources :posts, only: [:show] do
      resources :comments
      resources :votes, only: [:create, :destroy]
    end
  end
  resources :comments do
    resources :votes
  end
  resources :conversations, only: [:index, :show, :new, :create] do
    member do
      post :reply
      post :trash
      post :untrash
    end
  end
end
