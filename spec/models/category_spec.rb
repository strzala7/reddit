require 'rails_helper'

RSpec.describe Category, :type => :model do
end

# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  owner_id   :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_categories_on_owner_id  (owner_id)
#
