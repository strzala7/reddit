require 'rails_helper'

RSpec.describe Post, :type => :model do
  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:link) }

  let(:post) { FactoryGirl.create(:post) }
  let(:user) { FactoryGirl.create(:user) }

  it "has valid factory" do
    expect(post).to be_valid
  end

  context "validation" do
    it "checks link uniqueness" do
      valid_post = create(:post, title: "#{Faker::Lorem.sentence}", link: "#{Faker::Internet.url}", author: user)
      valid_post.valid?
      expect(valid_post).to be_valid
      invalid_post = build(:post, title: "#{Faker::Lorem.sentence}", link: valid_post.link, author: user)
      invalid_post.valid?
      expect(invalid_post.errors.messages[:link]).to include("has already been taken") 
    end
  end

  context "editable" do
    let(:post) { FactoryGirl.create(:post, author_id: user.id) }
    it "checks if user can edit post" do
      expect(post.editable?(user)).to eq(true)
      post.created_at = 2.minutes.ago
      expect(post.editable?(user)).to eq(false)
    end
  end

  context "scopes" do
    it ".main returns all posts with votes_differential > 1" do
      post1 = create(:post, votes_differential: 0)
      post2 = create(:post, votes_differential: 5)
      expect(Post.main.map(&:id)).to eq([post2.id])
    end

    it ".hot returns all posts with votes_differential > 10" do
      post1 = create(:post, votes_differential: 0)
      post2 = create(:post, votes_differential: 5)
      post3 = create(:post, votes_differential: 15)
      expect(Post.hot.map(&:id)).to eq([post3.id])
    end

    it ".latest returns all posts created_at > 24.hours.ago" do
      new_post = create(:post)
      old_post = create(:post, created_at: 48.hours.ago)
      expect(Post.latest.map(&:id)).to eq([new_post.id])
    end

    it ".current_month returns all posts created in the current month" do
      Timecop.freeze(Time.now.end_of_month) do
				@new_post = create(:post)
      	@three_weeks_ago = create(:post, created_at: 3.weeks.ago)
      	@five_weeks_ago = create(:post, created_at: 5.weeks.ago)
			end
		  expect(Post.current_month.map(&:id)).to match_array([@new_post.id, @three_weeks_ago.id])
    end

    it ".exclude_blacklisted returns posts added by people who are not in user's blacklist" do
      user = create(:user)
      blacklisted_user = create(:user)
      post = create(:post, author: blacklisted_user)
      expect(Post.exclude_blacklisted(user).map(&:id)).to eq([post.id])
      user.blacklisted << blacklisted_user
      expect(Post.exclude_blacklisted(user)).to be_empty
    end

    it ".from_favourite_categories returns posts from favourite categories" do
      user = create(:user)
      post = create(:post)
      expect(Post.from_favourite_categories(user)).to be_empty
      user.favourite_categories.create(category: post.category)
      expect(Post.from_favourite_categories(user).map(&:id)).to eq([post.id]) 
    end
  end
end

# == Schema Information
#
# Table name: posts
#
#  id                 :integer          not null, primary key
#  title              :text             not null
#  link               :string(255)      not null
#  author_id          :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#  votes_differential :integer          default(0), not null
#  category_id        :integer          not null
#  comments_count     :integer          default(0)
#
# Indexes
#
#  index_posts_on_author_id  (author_id)
#
