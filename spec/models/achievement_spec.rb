require 'rails_helper'

RSpec.describe Achievement, :type => :model do
end

# == Schema Information
#
# Table name: achievements
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  title      :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_achievements_on_user_id  (user_id)
#
