require 'rails_helper'

RSpec.describe Comment, :type => :model do
  it { should validate_presence_of(:body) }

  let(:comment) { FactoryGirl.create(:comment) }

  it "has valid factory" do
    expect(comment).to be_valid
  end

  describe "to_s" do
    it "returns body" do
      expect(comment.to_s).to eq(comment.body)
    end
  end
end

# == Schema Information
#
# Table name: comments
#
#  id              :integer          not null, primary key
#  body            :text             not null
#  post_id         :integer          not null
#  author_id       :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#  upvotes_count   :integer          default(0)
#  downvotes_count :integer          default(0)
#
# Indexes
#
#  index_comments_on_author_id  (author_id)
#  index_comments_on_post_id    (post_id)
#
