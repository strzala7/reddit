require 'rails_helper'

RSpec.describe Blacklist, :type => :model do
end

# == Schema Information
#
# Table name: blacklists
#
#  id             :integer          not null, primary key
#  user_id        :integer          not null
#  blacklisted_id :integer          not null
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_blacklists_on_blacklisted_id  (blacklisted_id)
#  index_blacklists_on_user_id         (user_id)
#
