require 'rails_helper'

RSpec.describe Ban, :type => :model do
  let(:ban) { FactoryGirl.create(:ban) }
  let(:user_1) { FactoryGirl.create(:user) }
  let(:user_2) { FactoryGirl.create(:user) }

  it "has valid factory" do
    expect(ban).to be_valid
  end

  context "validation" do
    it "checks if expiration date is not in past" do
      invalid_ban = build(:ban, person_of_authority: user_1,
                           user: user_2,
                           expiration: Time.now - 48.hours)
      invalid_ban.valid?
      expect(invalid_ban.errors.count).to eq(1) 
      expect(invalid_ban.errors.messages[:expiration]).to include("can't be in the past")
      valid_ban = invalid_ban
      valid_ban.expiration = Time.now + 48.hours
      valid_ban.valid?
      expect(valid_ban).to be_valid
    end
  end

  context "scopes" do
    before do
      user = create(:user)
      @banned_user = create(:user)
      @previous_ban = build(:ban, person_of_authority: user, user: @banned_user, expiration: Time.now - 24.hours, created_at: Time.now - 48.hours)
      @previous_ban2 = build(:ban, person_of_authority: user, user: @banned_user, expiration: Time.now - 12.hours, created_at: Time.now - 20.hours)
      @previous_ban.save(validate: false)
      @previous_ban2.save(validate: false)
      @current_ban = create(:ban, person_of_authority: user, user: @banned_user, expiration: Time.now + 24.hours)
    end

    it ".current returns user's current ban" do
      expect(@banned_user.bans.current).to eq(@current_ban)
    end

    it ".previous returns user's previous bans" do
			expect(@banned_user.bans.previous.size).to eq(2)
			expect(@banned_user.bans.previous).to match_array([@previous_ban, @previous_ban2])
		end
  end
end

# == Schema Information
#
# Table name: bans
#
#  id                     :integer          not null, primary key
#  person_of_authority_id :integer          not null
#  user_id                :integer          not null
#  expiration             :datetime
#  reason                 :text
#  created_at             :datetime
#  updated_at             :datetime
#  type                   :string(255)
#
# Indexes
#
#  index_bans_on_person_of_authority_id  (person_of_authority_id)
#  index_bans_on_user_id                 (user_id)
#
