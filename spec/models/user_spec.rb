require 'rails_helper'

RSpec.describe User, :type => :model do
  it { should validate_presence_of(:nickname) }
  it { should validate_uniqueness_of(:nickname) }
  it { should ensure_length_of(:nickname) }

  let(:user) { create(:user) }
  let(:user2) { create(:user) }

  it "has valid factory" do
    expect(user).to be_valid
  end

  describe "to_s" do
    it "returns nickname" do
      expect(user.to_s).to eq(user.nickname)
    end
  end

  describe "#spammer?" do
    before do
      @post = create(:post)
      @comment = create(:comment, author: user, post: @post)
    end

    it "checks if user waited 1min before adding next comment" do
      expect(user.spammer?(@post)).to eq(true)
      Timecop.freeze(Time.now + 1.minutes) do
        expect(user.spammer?(@post)).to eq(false)
      end
    end
  end

  describe "#followed_by?" do
    it "checks if user is followed by given user" do
      expect(user.followed_by?(user2)).to eq(false)
      user.followers << user2
      expect(user.followed_by?(user2)).to eq(true)
    end
  end

  describe "#following?" do
    it "checks if user is following given user" do
      expect(user.following?(user2)).to eq(false)
      user.followed_users << user2
      expect(user.following?(user2)).to eq(true)
    end
  end

  describe "#blacklisted?" do
    it "checks if user's blacklist includes given user" do
      expect(user.blacklisted?(user2)).to eq(false)
      user.blacklists.create(blacklisted: user2)
      expect(user.blacklisted?(user2)).to eq(true)
    end
  end

  describe "#banned?" do
    it "checks if user is currently banned" do
      Timecop.freeze(Time.now - 48.hours) do
        user.bans.create(person_of_authority: user2, reason: "reason", expiration: Time.now + 24.hours, type: 'normal')
      end
      expect(user.banned?).to eq(false)
      user.bans.create(person_of_authority: user2, reason: "reason", expiration: Time.now + 24.hours, type: 'normal')
      expect(user.banned?).to eq(true)
    end
  end

  describe "#has_in_favourites?" do
    it "checks if user has given category in favourites" do
      category = create(:category)
      expect(user.has_in_favourites?(category)).to eq(false)
      user.favourite_categories.create(category: category)
      expect(user.has_in_favourites?(category)).to eq(true)
    end
  end

end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime
#  updated_at             :datetime
#  nickname               :string(255)      not null
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  gender                 :string(255)
#  admin                  :boolean          default(FALSE)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
