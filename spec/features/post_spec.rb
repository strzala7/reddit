require 'rails_helper'

describe 'posts' do
	let(:user) { create(:user) }	

	context 'not signed in users' do
		it 'cannot begin to create new post' do
			visit root_path
			click_link('Submit a new link')
			expect(page).to have_content('You need to sign in or sign up before continuing.')
		end
  end

	context 'signed in users' do
		before do
			visit root_path
		  click_link('Sign in')
			fill_in('Email', with: user.email)
			fill_in('Password', with: user.password)
			click_button('Sign in')
			expect(page).to have_content('Signed in successfully')
		end

		it 'can begin to create new post' do
			visit root_path
			click_link('Submit a new link')
			expect(page).to have_button('Create Post')
		end
	end
end
