require 'rails_helper'

RSpec.describe 'authentication' do
  
	let(:user) { FactoryGirl.create(:user) }

	feature 'sign in' do 
		scenario 'user can sign in' do
			visit root_path
			click_link('Sign in')
			fill_in('Email', with: user.email)
			fill_in('Password', with: user.password)
			click_button('Sign in')
			expect(page).to have_content('Signed in successfully.')		
		end		

		scenario 'user cannot sign in if not registered' do
			visit root_path
			click_link('Sign in')
			fill_in('Email', with: 'notregistereduser@o2.pl')
			fill_in('Password', with: 'nonregistereduserpass')
			click_button('Sign in')
			expect(page).to have_content('Invalid email address or password.')
		end
	end
end

