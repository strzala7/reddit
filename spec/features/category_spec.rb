require 'rails_helper'

describe 'category' do
	context 'not signed in users' do
		it 'cannot begin to add category' do
			visit root_path
			click_button 'Add category'
			expect(page).to have_content('You need to sign in or sign up before continuing.')	
		end	
	end

	context 'signed in user' do
		before do
			visit root_path
		  click_link('Sign in')
			fill_in('Email', with: user.email)
			fill_in('Password', with: user.password)
			click_button('Sign in')
			expect(page).to have_content('Signed in successfully')
		end

		it 'can add category' do
			visit root_path
			click_button('Add category')
						
		end
	end
end
