require 'rails_helper'

describe 'comments' do
	let(:user) { create(:user) }
	let(:post) { create(:post) }	

	context 'not signed in users' do
		it 'cannot add comment' do
			visit categories_show_post_path(post.category.name, post)
			fill_in 'comment[body]', with: 'comment123'
			click_button 'Save'
			expect(page).to have_content('You need to sign in or sign up before continuing.')
		end
	end

	context 'signed in users' do		 
		before do
			visit root_path
		  click_link('Sign in')
			fill_in('Email', with: user.email)
			fill_in('Password', with: user.password)
			click_button('Sign in')
			expect(page).to have_content('Signed in successfully')
		end

		it 'can add comment' do
			visit categories_show_post_path(post.category.name, post)
			fill_in 'comment[body]', with: 'comment123'
			click_button 'Save'
			expect(page).to have_content('comment123')
  	end
	end
end
