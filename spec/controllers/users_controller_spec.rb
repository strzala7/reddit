require 'rails_helper'

RSpec.describe UsersController, :type => :controller do
  render_views
  let(:user) { FactoryGirl.create(:user) }

  before(:each) do
    sign_in user
  end

  describe "show" do
    before { get :show, id: user }

    it "responds successfully with HTTP 200 status code" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "should display nickname on view" do
      expect(response.body).to match(user.nickname)
    end
  end
end