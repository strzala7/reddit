require 'rails_helper'

RSpec.describe PostsController, :type => :controller do
  render_views
  before(:each) do
    sign_in create(:user)
  end

  describe "index" do
    before do
      create(:post, title: "first")
      create(:post, title: "second")
    end

    it "responds successfully with HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "exposes posts" do
      get :index
      expect(controller.posts.count).to eq(2)
    end

    it "should display posts on view" do
      get :index
      expect(response.body).to match /first/
      expect(response.body).to match /second/
    end
  end

  describe "show" do
		subject { create(:post) }
		before { get :show, id: subject, category_name: subject.category }
    it "responds successfully with HTTP 200 status code" do
			expect(response).to be_success
      expect(response.status).to eq(200)
    end
  end

  describe "new" do
    before { get :new }

    it "responds successfully with HTTP 200 status code" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "exposes new project" do
      expect(controller.post.created_at).to be_nil
    end
  end

  describe "create" do
    before { get :create }

    it "responds successfully with HTTP 200 status code" do
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    context "with valid attributes" do
      it "creates a new post" do
        expect(controller.posts.count).to eq(0)
        create(:post)
        expect(controller.posts.count).to eq(1)
      end
    end
  end

  describe "edit" do
    context "as author" do
      before do
        @user = create(:user)
        sign_in @user
      end
      subject { create(:post, author_id: @user.id) }
      before { get :edit, id: subject }

      it "responds successfully with HTTP 200 status code" do
        expect(response).to be_success
        expect(response.status).to eq(200)
      end

      context "not author" do
        subject { create(:post) }
        before { get :edit, id: subject }

        it "redirects to posts#index" do
          expect(response.status).to eq(302)
          expect(response).to redirect_to :root
        end
      end
    end
  end

end
