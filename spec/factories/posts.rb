# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post do
    title { Faker::Lorem.sentence }
    link { Faker::Internet.url }
    author { association(:user) }
    votes_differential { Faker::Number.number(3) }
    category { association(:category) }

    factory :invalid_post do
      title nil
    end
  end
end

# == Schema Information
#
# Table name: posts
#
#  id                 :integer          not null, primary key
#  title              :text             not null
#  link               :string(255)      not null
#  author_id          :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#  votes_differential :integer          default(0), not null
#  category_id        :integer          not null
#  comments_count     :integer          default(0)
#
# Indexes
#
#  index_posts_on_author_id  (author_id)
#
