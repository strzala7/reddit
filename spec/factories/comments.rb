# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    body { Faker::Lorem.paragraph }
    post { association(:post) }
    author { association(:user) }
  end
end

# == Schema Information
#
# Table name: comments
#
#  id              :integer          not null, primary key
#  body            :text             not null
#  post_id         :integer          not null
#  author_id       :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#  upvotes_count   :integer          default(0)
#  downvotes_count :integer          default(0)
#
# Indexes
#
#  index_comments_on_author_id  (author_id)
#  index_comments_on_post_id    (post_id)
#
