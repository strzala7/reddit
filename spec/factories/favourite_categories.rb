# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :favourite_category do
    user nil
    category nil
  end
end

# == Schema Information
#
# Table name: favourite_categories
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  category_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_favourite_categories_on_category_id  (category_id)
#  index_favourite_categories_on_user_id      (user_id)
#
