# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :following_relationship do
    follower nil
    followed_user nil
  end
end

# == Schema Information
#
# Table name: following_relationships
#
#  id               :integer          not null, primary key
#  follower_id      :integer          not null
#  followed_user_id :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_following_relationships_on_followed_user_id  (followed_user_id)
#  index_following_relationships_on_follower_id       (follower_id)
#
