# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ban do
    person_of_authority { association(:user) }
    user { association(:user) }
    expiration  Time.now + 48.hours
    reason { Faker::Lorem.paragraph }
		type { %w(normal shadowban).sample }
  end
end

# == Schema Information
#
# Table name: bans
#
#  id                     :integer          not null, primary key
#  person_of_authority_id :integer          not null
#  user_id                :integer          not null
#  expiration             :datetime
#  reason                 :text
#  created_at             :datetime
#  updated_at             :datetime
#  type                   :string(255)
#
# Indexes
#
#  index_bans_on_person_of_authority_id  (person_of_authority_id)
#  index_bans_on_user_id                 (user_id)
#
