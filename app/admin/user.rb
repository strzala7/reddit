ActiveAdmin.register User do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end

    permit_params [:nickname, :email, :gender, :password, :password_confirmation] 

  form do |f|
      f.inputs "User" do
        f.input :nickname
        f.input :email
        f.input :gender, collection: %w(male female) 
        f.input :password
        f.input :password_confirmation
      end
      f.actions
  end

  index do
    column :id
    column :nickname
    column :email
    column :admin
    column :created_at
    column :updated_at
    actions
  end


end
