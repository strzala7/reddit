ActiveAdmin.register Post do


  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #   permitted = [:permitted, :attributes]
  #   permitted << :other if resource.something?
  #   permitted
  # end


  index do
    column :id
    column 'Title' do |post|
      post.title.truncate(50)
    end
    column 'Link' do |post|
      link_to post.link.truncate(50), url_for(post.link)
    end
    column :author
    column :votes_differential
    column :category
    column :comments_count
    column :created_at
    column :updated_at
    actions
  end

  #
#  id                 :integer          not null, primary key
#  title              :text             not null
#  link               :string(255)      not null
#  author_id          :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#  votes_differential :integer          default(0), not null
#  category_id        :integer          not null
#  comments_count     :integer          default(0)


end
