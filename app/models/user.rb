class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  acts_as_messageable

  has_many :posts, foreign_key: 'author_id'
  has_many :comments, foreign_key: 'author_id'
  has_many :votes
  has_many :followed_relationships, class_name: FollowingRelationship, foreign_key: 'follower_id'
  has_many :followed_users, through: :followed_relationships
  has_many :follower_relationships, class_name: FollowingRelationship, foreign_key: 'followed_user_id'
  has_many :followers, through: :follower_relationships
  has_many :achievements
  has_many :blacklists
  has_many :blacklisted, through: :blacklists
  has_many :bans
  has_many :categories, foreign_key: 'owner_id'

  has_many :favourite_categories
  has_many :favourites, through: :favourite_categories, source: :category

  has_attached_file :avatar, styles: { medium: "150x150!", thumb: "40x40!" }, default_url: "default-:style.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  validates :nickname, presence: true,
                       length: { in: 3..20 },
                       uniqueness: true
  validates :gender, inclusion: { in: %w(undefined male female) },
                     allow_nil: true

  scope :shadow_banned, -> { joins(:bans).merge(Ban.where(type: 'shadowban')) }

  def to_s
    nickname
  end

  def spammer? post
    unless comments.where(post_id: post.id).last.nil?
      comments.where(post_id: post.id).last.created_at > 1.minutes.ago
    end
  end

  def followed_by? user
    followers.where(id: user.id).present?
  end

  def following? user
    followed_users.where(id: user.id).present?
  end

  def blacklisted? user
    blacklisted.where(id: user).present?
  end

  def banned? type='normal'
    bans.where('expiration > ? and type = ?', Time.now, type).present?
  end

  def has_achievement? achievement
    achievements.where(title: achievement).present?
  end

  def has_in_favourites? category
    favourites.where(id: category.id).present?
  end
end

# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime
#  updated_at             :datetime
#  nickname               :string(255)      not null
#  avatar_file_name       :string(255)
#  avatar_content_type    :string(255)
#  avatar_file_size       :integer
#  avatar_updated_at      :datetime
#  gender                 :string(255)
#  admin                  :boolean          default(FALSE)
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
