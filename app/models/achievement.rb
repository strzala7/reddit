class Achievement < ActiveRecord::Base
  belongs_to :user

  #za rok od rejestracji
  def self.year
    users = User.where('created_at <= ?', 1.years.ago)
    assign_achievement(users, 'year')
  end

  #dla osoby, która miała bana. Ale już nie ma :)
  def self.banana
    users = User.joins(:bans).merge(Ban.previous)
    assign_achievement(users, 'banana')
  end

  #za liczbę znalezisk na stronie głównej
  def self.redditor
    users = User.joins(:posts).merge(Post.main).group('users.id').having('count(posts.id) > 1')
    assign_achievement(users, 'redditor')
  end

  #za liczbę napisanych komentarzy w znaleziskach
  def self.commentator
    users = User.joins(:comments).group('users.id').having("count(comments.id) > 1")
    assign_achievement(users, 'commentator')
  end

  #za znalezisko z największą liczbą votes_differential w miesiącu
  def self.man_of_the_month
    users = User.joins(:posts).merge(Post.current_month.order(votes_differential: :desc).limit(1))
    assign_achievement(users, 'man_of_the_month')
  end

  #za znalezisko, które weszły na główną w 'hot'
  def self.lord_of_fire
    User.joins(:posts).merge(Post.hot)
    assign_achievement(users, 'lord_of_fire')
  end

  #za liczbę obserwujących osób
  def self.vip
    User.joins(:followers).group('users.id').having('count(follower_id) > 1')
    assign_achievement(users, 'vip')
  end

  def self.assign_achievement users, achievement
    users.each do |user|
      unless user.has_achievement?(achievement)
        Achievement.create(title: achievement, user: user)
      end
    end
  end

end

# == Schema Information
#
# Table name: achievements
#
#  id         :integer          not null, primary key
#  user_id    :integer          not null
#  title      :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_achievements_on_user_id  (user_id)
#
