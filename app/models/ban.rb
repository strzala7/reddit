class Ban < ActiveRecord::Base
  self.inheritance_column = nil
  belongs_to :person_of_authority, class_name: User
  belongs_to :user

  validates :type, inclusion: { in: %w(normal shadowban) }
  validate :end_date_cannot_be_in_past

  scope :current,  -> { where('expiration > ?', Time.now).first}
  scope :previous, -> { where('expiration < ?', Time.now) }  

  def end_date_cannot_be_in_past
    errors.add(:expiration, "can't be in the past") if expiration < Date.today
  end
end

# == Schema Information
#
# Table name: bans
#
#  id                     :integer          not null, primary key
#  person_of_authority_id :integer          not null
#  user_id                :integer          not null
#  expiration             :datetime
#  reason                 :text
#  created_at             :datetime
#  updated_at             :datetime
#  type                   :string(255)
#
# Indexes
#
#  index_bans_on_person_of_authority_id  (person_of_authority_id)
#  index_bans_on_user_id                 (user_id)
#
