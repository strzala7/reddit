class Category < ActiveRecord::Base
  belongs_to :owner, class_name: User
  has_many :posts

  validates :name, presence: true
  validate :ensure_correct_character

  def to_s
    name
  end

  def ensure_correct_character
    name.try(:each_char) do |char|
      errors.add(:name, "can only contain letters and '-'") unless (('a'..'z').to_a<<('A'..'Z').to_a<<')').flatten.include?(char)
    end
  end
end

# == Schema Information
#
# Table name: categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  owner_id   :integer
#  created_at :datetime
#  updated_at :datetime
#
# Indexes
#
#  index_categories_on_owner_id  (owner_id)
#
