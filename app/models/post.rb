class Post < ActiveRecord::Base
  belongs_to :author, class_name: 'User', foreign_key: 'author_id'
  belongs_to :category
  has_many :comments
  has_many :votes, as: :votable

  validates :title, presence: true
  validates :link, presence: true,
                   uniqueness: true,
                   url: true

  scope :main,                            -> { where('votes_differential > 1') }
  scope :hot,                             -> { where('votes_differential > 10') }
  scope :latest,                          -> { where('created_at > ?', 24.hours.ago) }
  scope :current_month,                   -> { where('EXTRACT(MONTH from posts.created_at) > ?', Time.now.month-1) }
  scope :exclude_blacklisted,       ->(user) { where.not(author_id: user.blacklisted) }
  scope :from_favourite_categories, ->(user) { where(category_id: user.favourites) }

  def editable? user
    author == user && comments.count == 0 && created_at > 1.minutes.ago
  end
  
  def calculate_votes
    positive = self.votes.where(value: 1).count
    negative = self.votes.where(value: -1).count
    positive - negative
  end
end

# == Schema Information
#
# Table name: posts
#
#  id                 :integer          not null, primary key
#  title              :text             not null
#  link               :string(255)      not null
#  author_id          :integer          not null
#  created_at         :datetime
#  updated_at         :datetime
#  votes_differential :integer          default(0), not null
#  category_id        :integer          not null
#  comments_count     :integer          default(0)
#
# Indexes
#
#  index_posts_on_author_id  (author_id)
#
