class Comment < ActiveRecord::Base
  belongs_to :post, counter_cache: true
  belongs_to :author, class_name: 'User', foreign_key: 'author_id'
  has_many :votes, as: :votable

  validates :body, presence: true

  scope :exclude_shadowbanned, -> { where.not(author: User.shadow_banned) }

  def to_s
    body
  end

  def calculate_votes
    positive = self.votes.where(value: 1).count
    negative = self.votes.where(value: -1).count
    positive - negative
  end
end

# == Schema Information
#
# Table name: comments
#
#  id              :integer          not null, primary key
#  body            :text             not null
#  post_id         :integer          not null
#  author_id       :integer          not null
#  created_at      :datetime
#  updated_at      :datetime
#  upvotes_count   :integer          default(0)
#  downvotes_count :integer          default(0)
#
# Indexes
#
#  index_comments_on_author_id  (author_id)
#  index_comments_on_post_id    (post_id)
#
