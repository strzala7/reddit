class FavouriteCategory < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
end

# == Schema Information
#
# Table name: favourite_categories
#
#  id          :integer          not null, primary key
#  user_id     :integer
#  category_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#
# Indexes
#
#  index_favourite_categories_on_category_id  (category_id)
#  index_favourite_categories_on_user_id      (user_id)
#
