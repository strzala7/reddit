class FollowingRelationshipsController < ApplicationController
  before_action :authenticate_user!
  before_action :deny_access, unless: :can_follow?
  expose(:user)

  def create
    current_user.followed_users << user
    redirect_to :back
  end

  def destroy
    current_user.followed_users.delete(user)
    redirect_to :back
  end

  private

  def can_follow?
    user != current_user && !current_user.following?(user)
  end
end
