class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :deny_access, if: :spammer?
  expose(:post)
  expose(:comment, attributes: :comment_params)

  def create
    comment.author = current_user
    comment.post = post
    comment.save
    redirect_to categories_show_post_path(post.category, post)
  end

  private

  def comment_params
    params.require(:comment).permit(:body)
  end

  def spammer?
    current_user.spammer?(post)
  end
end
