class VotesController < ApplicationController
  before_action :authenticate_user!

  def create
    if request.path.include?('comment')
      resource, id = request.path.split('/')[1,2]
    else
      resource, id = request.path.split('/')[2..3]
    end
    votable = resource.singularize.classify.constantize.find(id)
    vote = votable.votes.build(value: params[:value], user_id: current_user.id)
    unless vote.exists?(vote.votable_type, vote.votable_id, vote.user)
      vote.save
      recalculate_votes(vote)
      recalculate_comment_votes_count(vote)
    end
    redirect_to :back
  end

  private

  def recalculate_votes vote
    return false unless vote.votable_type == 'Post'
    post = Post.where(id: vote.votable_id).first
    post.update(votes_differential: post.calculate_votes)
  end

  def recalculate_comment_votes_count vote
    return false unless vote.votable_type == 'Comment'
    comment = vote.votable
    comment.update(
      upvotes_count: comment.votes.where(value: 1).count,
      downvotes_count: comment.votes.where(value: -1).count
      )
  end
end
