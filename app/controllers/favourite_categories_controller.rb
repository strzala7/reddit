class FavouriteCategoriesController < ApplicationController
  expose(:category) { Category.find_by_name(params[:category_name]) }

  def create
    current_user.favourites << category
    redirect_to :back
  end

  def destroy
    current_user.favourites.delete(category)
    redirect_to :back
  end
end
