class PostsController < ApplicationController
  respond_to :html

  before_action :authenticate_user!, except: [:index, :show, :hot, :latest, :main]
  before_action :deny_access, unless: :editable?, only: [:edit, :update, :destroy]
  expose(:post, attributes: :post_params)
  expose(:posts) { Post.order('created_at DESC').page params[:page] }
  expose(:comment) { Comment.new }

  def create
    post.author = current_user
    if post.save
      redirect_to categories_show_post_path(post.category.name, post)
    else
      render :new
    end
  end

  def update
    if post.save
      redirect_to categories_show_post_path(post.category.name, post)
    else
      render :edit
    end
  end

  private

  def post_params
    params.require(:post).permit(:title, :link, :category_id)
  end

  def editable?
    post.editable?(current_user)
  end
end
