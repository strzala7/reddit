class BlacklistsController < ApplicationController
  before_action :authenticate_user!
  expose(:user)

  def create
    current_user.blacklists.create(blacklisted: user)
    redirect_to :back
  end

  def destroy
    current_user.blacklisted.delete(user)
    redirect_to :back
  end
end
