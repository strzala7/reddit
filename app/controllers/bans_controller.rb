class BansController < ApplicationController
  expose(:ban, attributes: :ban_params)
  expose(:user)

  def create
    ban.person_of_authority = current_user
    ban.user = user
    if ban.save
      redirect_to user_path(user)
    else
      render :new
    end
  end

  private

  def ban_params
    params.require(:ban).permit(:expiration, :type)
  end
end
