class CategoriesController < ApplicationController
  expose(:category) { Category.find_by_name(params[:category_name]) }
end
