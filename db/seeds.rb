# Post.delete_all
# Category.delete_all
# Comment.delete_all
# User.delete_all

class DataFetcher
  def scrape url, number_of_pages
    return if number_of_pages == 0
    mechanize = Mechanize.new
    page = mechanize.get(url)

    page.search('.sitetable .thing').each_with_index do |item, index|
      votes_differential = item.search('.midcol.unvoted .score.unvoted').text.strip
      title = item.search('.title .may-blank').text.strip
      link = item.search('.title .may-blank').first['href']
      author = item.search('.author').text.strip
      category = item.search('.subreddit').text.strip[3..-1]
      category_owner = find_category_owner('http://www.reddit.com/r/' + category + '/about/moderators')
      post_comments_url = item.search('.comments').first['href']
      domain = item.search('.domain').text
      link = 'http://www.reddit.com/' + link if domain.include?('self.')

      puts set_user(category_owner)
      puts set_user(author)
      puts set_category(category, category_owner)
      puts set_post(title, link, votes_differential, author, category)

      scrape_comments(title, post_comments_url)
    end

    next_page = page.search(".nextprev a:contains('next ›')").first['href']
    number_of_pages -= 1
    scrape(next_page, number_of_pages)
  end

  def scrape_comments post_title, post_comments_url
    page = Mechanize.new.get(post_comments_url)

    page.search('.commentarea>.sitetable.nestedlisting>.thing.comment>.entry').each do |item|
      unless item.search('em').text == '[deleted]'
        author = item.search('.author').text.strip
        body = item.search('.md').text
        
        puts set_user(author)
        puts set_comment(body, author, post_title)
      end
    end
  end

  def find_category_owner url
    moderators_page = Nokogiri::HTML(open(url))
    moderators_page.search('tr td .user a').first.text.strip
  end

  def set_user username
    User.find_or_create_by(nickname: username) do |user|
      user.email = username + "@mail.com"
      user.gender = %w(undefined male female).sample
      user.password = username + '_pass'
    end
  end

  def set_category name, category_owner
    Category.find_or_create_by(name: name) do |category|
      category.owner = User.where(nickname: category_owner).first
    end
  end

  def set_post title, link, votes_differential, author, category
    Post.find_or_create_by(title: title) do |post|
      category == 'AskReddit' ? post.link = 'http://www.reddit.com/' + link : post.link = link
      post.votes_differential = votes_differential
      post.author = User.where(nickname: author).first
      post.category = Category.where(name: category).first
    end   
  end

  def set_comment body, author, post_title
    Comment.find_or_create_by(
      body: body,
      author_id: User.where(nickname: author).first.id,
      post: Post.where(title: post_title).first
      )
  end

end


d = DataFetcher.new
d.scrape('http://www.reddit.com/?count=325&after=t3_2l1b51', 500)