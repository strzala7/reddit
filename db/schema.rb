# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141104001901) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "achievements", force: true do |t|
    t.integer  "user_id",    null: false
    t.string   "title",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "achievements", ["user_id"], name: "index_achievements_on_user_id", using: :btree

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "bans", force: true do |t|
    t.integer  "person_of_authority_id", null: false
    t.integer  "user_id",                null: false
    t.datetime "expiration"
    t.text     "reason"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
  end

  add_index "bans", ["person_of_authority_id"], name: "index_bans_on_person_of_authority_id", using: :btree
  add_index "bans", ["user_id"], name: "index_bans_on_user_id", using: :btree

  create_table "blacklists", force: true do |t|
    t.integer  "user_id",        null: false
    t.integer  "blacklisted_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "blacklists", ["blacklisted_id"], name: "index_blacklists_on_blacklisted_id", using: :btree
  add_index "blacklists", ["user_id"], name: "index_blacklists_on_user_id", using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.integer  "owner_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories", ["owner_id"], name: "index_categories_on_owner_id", using: :btree

  create_table "comments", force: true do |t|
    t.text     "body",                        null: false
    t.integer  "post_id",                     null: false
    t.integer  "author_id",                   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "upvotes_count",   default: 0
    t.integer  "downvotes_count", default: 0
  end

  add_index "comments", ["author_id"], name: "index_comments_on_author_id", using: :btree
  add_index "comments", ["post_id"], name: "index_comments_on_post_id", using: :btree

  create_table "favourite_categories", force: true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favourite_categories", ["category_id"], name: "index_favourite_categories_on_category_id", using: :btree
  add_index "favourite_categories", ["user_id"], name: "index_favourite_categories_on_user_id", using: :btree

  create_table "following_relationships", force: true do |t|
    t.integer  "follower_id",      null: false
    t.integer  "followed_user_id", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "following_relationships", ["followed_user_id"], name: "index_following_relationships_on_followed_user_id", using: :btree
  add_index "following_relationships", ["follower_id"], name: "index_following_relationships_on_follower_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: true do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: true do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "mailboxer_notifications", force: true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: true do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "posts", force: true do |t|
    t.text     "title",                          null: false
    t.string   "link",                           null: false
    t.integer  "author_id",                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "votes_differential", default: 0, null: false
    t.integer  "category_id",                    null: false
    t.integer  "comments_count",     default: 0
  end

  add_index "posts", ["author_id"], name: "index_posts_on_author_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "nickname",                               null: false
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "gender"
    t.boolean  "admin",                  default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "value",        null: false
    t.integer  "votable_id",   null: false
    t.string   "votable_type", null: false
    t.integer  "user_id",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["user_id"], name: "index_votes_on_user_id", using: :btree
  add_index "votes", ["votable_id", "votable_type"], name: "index_votes_on_votable_id_and_votable_type", using: :btree

  add_foreign_key "achievements", "users", name: "achievements_user_id_fk"

  add_foreign_key "bans", "users", name: "bans_person_of_authority_id_fk", column: "person_of_authority_id"
  add_foreign_key "bans", "users", name: "bans_user_id_fk"

  add_foreign_key "blacklists", "users", name: "blacklists_blacklisted_id_fk", column: "blacklisted_id"
  add_foreign_key "blacklists", "users", name: "blacklists_user_id_fk"

  add_foreign_key "categories", "users", name: "categories_owner_id_fk", column: "owner_id"

  add_foreign_key "comments", "posts", name: "comments_post_id_fk"
  add_foreign_key "comments", "users", name: "comments_author_id_fk", column: "author_id"

  add_foreign_key "favourite_categories", "categories", name: "favourite_categories_category_id_fk"
  add_foreign_key "favourite_categories", "users", name: "favourite_categories_user_id_fk"

  add_foreign_key "following_relationships", "users", name: "following_relationships_followed_user_id_fk", column: "followed_user_id"
  add_foreign_key "following_relationships", "users", name: "following_relationships_follower_id_fk", column: "follower_id"

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", name: "mb_opt_outs_on_conversations_id", column: "conversation_id"

  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", name: "notifications_on_conversation_id", column: "conversation_id"

  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", name: "receipts_on_notification_id", column: "notification_id"

  add_foreign_key "posts", "categories", name: "posts_category_id_fk"
  add_foreign_key "posts", "users", name: "posts_author_id_fk", column: "author_id"

  add_foreign_key "votes", "users", name: "votes_user_id_fk"

end
