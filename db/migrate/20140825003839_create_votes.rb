class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.integer :value, null: false
      t.belongs_to :votable, polymorphic: true, null: false
      t.belongs_to :user, null: false, index: true

      t.timestamps
    end
    add_index :votes, [:votable_id, :votable_type]
  end
end
