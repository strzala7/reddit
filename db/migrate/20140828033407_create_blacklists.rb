class CreateBlacklists < ActiveRecord::Migration
  def change
    create_table :blacklists do |t|
      t.belongs_to :user, null: false, index: true
      t.belongs_to :blacklisted, null: false, index: true

      t.timestamps
    end
  end
end
