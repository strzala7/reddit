class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :title, null: false
      t.string :link, null: false
      t.belongs_to :author, null: false, index: true

      t.timestamps
    end
  end
end
