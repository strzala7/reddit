class AddKeys < ActiveRecord::Migration
  def change
    add_foreign_key "posts", "users", name: "posts_author_id_fk", column: "author_id"
  end
end
