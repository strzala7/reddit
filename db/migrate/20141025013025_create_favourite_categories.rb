class CreateFavouriteCategories < ActiveRecord::Migration
  def change
    create_table :favourite_categories do |t|
      t.belongs_to :user, index: true
      t.belongs_to :category, index: true

      t.timestamps
    end
  end
end
