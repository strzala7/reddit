class AddVotesCountToComments < ActiveRecord::Migration
  def self.up
    add_column :comments, :upvotes_count, :integer, default: 0
    add_column :comments, :downvotes_count, :integer, default: 0

    Comment.find_each do |comment|
      comment.update_columns(
        upvotes_count: comment.votes.where(value: 1).count,
        downvotes_count: comment.votes.where(value: -1).count
        )
    end
  end

  def self.down
    remove_column :comments, :upvotes_count
    remove_column :comments, :downvotes_count
  end
end
