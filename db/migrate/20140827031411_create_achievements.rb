class CreateAchievements < ActiveRecord::Migration
  def change
    create_table :achievements do |t|
      t.belongs_to :user, null: false, index: true
      t.string :title, null: false

      t.timestamps
    end
  end
end
