class AddCommentsCountToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :comments_count, :integer, default: 0

    Post.find_each do |post|
      Post.reset_counters(post.id, :comments)
    end
  end

  def self.down
    remove_column :posts, :comments_count
  end
end
