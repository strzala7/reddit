class CreateFollowingRelationships < ActiveRecord::Migration
  def change
    create_table :following_relationships do |t|
      t.belongs_to :follower, null: false, index: true
      t.belongs_to :followed_user, null: false, index: true

      t.timestamps
    end
  end
end
