class AddKeys2 < ActiveRecord::Migration
  def change
    add_foreign_key "achievements", "users", name: "achievements_user_id_fk"
    add_foreign_key "bans", "users", name: "bans_person_of_authority_id_fk", column: "person_of_authority_id"
    add_foreign_key "bans", "users", name: "bans_user_id_fk"
    add_foreign_key "blacklists", "users", name: "blacklists_blacklisted_id_fk", column: "blacklisted_id"
    add_foreign_key "blacklists", "users", name: "blacklists_user_id_fk"
    add_foreign_key "categories", "users", name: "categories_owner_id_fk", column: "owner_id"
    add_foreign_key "comments", "users", name: "comments_author_id_fk", column: "author_id"
    add_foreign_key "comments", "posts", name: "comments_post_id_fk"
    add_foreign_key "favourite_categories", "categories", name: "favourite_categories_category_id_fk"
    add_foreign_key "favourite_categories", "users", name: "favourite_categories_user_id_fk"
    add_foreign_key "following_relationships", "users", name: "following_relationships_followed_user_id_fk", column: "followed_user_id"
    add_foreign_key "following_relationships", "users", name: "following_relationships_follower_id_fk", column: "follower_id"
    add_foreign_key "posts", "categories", name: "posts_category_id_fk"
    add_foreign_key "votes", "users", name: "votes_user_id_fk"
  end
end
