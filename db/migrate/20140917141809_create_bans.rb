class CreateBans < ActiveRecord::Migration
  def change
    create_table :bans do |t|
      t.belongs_to :person_of_authority, null: false, index: true
      t.belongs_to :user, null: false, index: true
      t.datetime :expiration
      t.text :reason

      t.timestamps
    end
  end
end
