class AddVotesDifferentialToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :votes_differential, :integer, null: false, default: 0
  end
end
